package com.axis.bookinventory

import com.axis.bookinventory.controller.BookInventoryController
import com.axis.bookinventory.model.Book
import com.axis.bookinventory.model.ImageLinks
import com.axis.bookinventory.repository.BookRepository
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.Assert.*
import org.junit.runner.RunWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.junit.MockitoJUnitRunner
import reactor.test.StepVerifier
import kotlin.collections.ArrayList
import reactor.core.publisher.toFlux
import reactor.core.publisher.toMono

@RunWith(MockitoJUnitRunner::class)
class BooksControllerTest {

    @InjectMocks
    lateinit var booksController: BookInventoryController

    @Mock
    lateinit var booksRepository: BookRepository

    var bookList : ArrayList<Book> = ArrayList()


    @Before
    fun setUp() {
        bookList.add(Book(200,"Are you afraid of the Dark", listOf("Sidney Sheldon","CJ"), ImageLinks(""),"IIT Life",90.5,4))
    }

    @After
    fun tearDown() {

    }

    @Test
    fun shouldInsertBookIntoBooksRepository() {

        doReturn(bookList[0].toMono()).`when`(booksRepository).save(bookList[0])
        var result = booksController.addBook(bookList[0])
        verify(booksRepository,times(1)).save(bookList[0])
        StepVerifier
                .create<Any>(result)
                .assertNext { book ->
                    assertNotNull(book)
                }
                .expectComplete()
                .verify()

        assertNotNull(result)
    }


    @Test
    fun shouldGetAllBooksFromBooksRepository() {

        doReturn(bookList.toFlux()).`when`(booksRepository).findAll()
        var result  = booksController.getAllBooks()
        verify(booksRepository,times(1)).findAll()
        assertNotNull(result)

        StepVerifier
                .create<Any>(result)
                .assertNext { book ->
                    assertNotNull(book)
//                    assertEquals(bookList[0].id, book.)
                }
                .expectComplete()
                .verify()
    }

    @Test
    fun shouldGetBookFromBooksRepositoryById() {

        doReturn(bookList[0].toMono()).`when`(booksRepository).findById(200)
        var result  = booksController.getBook(200)
        verify(booksRepository,times(1)).findById(200)
        assertNotNull(result)

        StepVerifier
                .create<Any>(result)
                .assertNext { book ->
                    assertNotNull(book)
//                    assertEquals(bookList[0].id, book.)
                }
                .expectComplete()
                .verify()
    }

    @Test
    fun shouldUpdateBookInBooksRepositoryById() {
        doReturn(bookList[0].toMono()).`when`(booksRepository).findById(100)
        doReturn(bookList[0].toMono()).`when`(booksRepository).save(bookList[0])
        var result  = booksController.editBook(100,bookList[0])
        assertNotNull(result)

        StepVerifier
                .create<Any>(result)
                .assertNext { book ->
                    assertNotNull(book)
                }
                .expectComplete()
                .verify()
    }

/*    @Test
    fun shouldDeleteBookInBooksRepositoryById() {
        doReturn(bookList[0].toMono()).`when`(booksRepository).findById(bookList[0].id)
        var deleteResult : Mono<Void> = Mono.empty()
        doReturn(deleteResult).`when`(booksRepository).delete(bookList[0])
        var result  = booksController.deleteBook(bookList[0].id)
        verify(booksRepository,times(1)).delete(bookList[0])
        assertNotNull(result)
    }
*/
}