package com.axis.bookinventory

import com.axis.bookinventory.controller.BookInventoryController
import com.axis.bookinventory.model.Book
import com.axis.bookinventory.model.ImageLinks
import com.axis.bookinventory.repository.BookRepository
import com.axis.bookinventory.service.GoogleBookService
import org.junit.After
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.test.web.reactive.server.WebTestClient
import org.springframework.web.reactive.function.BodyInserters

@ExtendWith(SpringExtension::class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class BookInventoryIntegrationTests{

    lateinit var webTestClient: WebTestClient
    @Autowired
    lateinit var bookRepository:BookRepository

    @Autowired
    lateinit var bookService:GoogleBookService

    var expectedList: List<Book> = emptyList()

    @BeforeEach
    fun beforeEach(){
        webTestClient = WebTestClient
                .bindToController(BookInventoryController(bookRepository,bookService))
                .configureClient()
                .baseUrl("/books")
                .build()
    //    expectedList = listOf(Book(1,"Are you afraid of the Dark", listOf("Sidney Sheldon","CJ"),"http://localhost:8080/456","IIT Life",90.5,4))
        this.expectedList = bookRepository.findAll().collectList().block()!!
    }

    @Test
    fun testGetAllBooks(){
        webTestClient
                .get()
                .uri("/")
                .exchange()
                .expectStatus()
                .isOk
                .expectBodyList(Book::class.java)
                .isEqualTo<WebTestClient.ListBodySpec<Book>>(expectedList.toList())
    }

    @Test
    fun testSearchWithinBooks(){
        webTestClient
                .get()
                .uri("/search?key={key}","Thriller")
                .exchange()
                .expectStatus()
                .isOk
                .expectBodyList(Book::class.java)
                .hasSize(2)
    }

    @Test
    fun testBookIdInvalidNotFound(){
        webTestClient
                .get()
                .uri("/88")
                .exchange()
                .expectStatus()
                .isNotFound
    }

    @Test
    fun testBookIdValidFound(){
        var expectedBook:List<Book> = listOf(expectedList.first())
        webTestClient
                .get()
                .uri("/{id}",expectedBook.first().id)
                .exchange()
                .expectStatus()
                .isOk
                .expectBodyList(Book :: class.java)
                .isEqualTo<WebTestClient.ListBodySpec<Book>>(expectedBook)
    }

    @Test
    fun testEditBook(){
        val expectedBook:Book = expectedList.first()
       webTestClient.put().uri("/{id}", expectedBook.id)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromObject(expectedBook))
                .exchange()
                .expectStatus().isOk
                .expectBody(Book::class.java)
    }

    @Test
    fun testSaveBook() {
        val book : Book = Book(500,"Bloodline", listOf("Author 1","CJ"), ImageLinks("image 2"),"IIT Life",90.5,4)
        webTestClient.post().uri("/")
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromObject(book))
                .exchange()
                .expectStatus().isCreated
                .expectBody(Book::class.java)
    }

    @After
    fun tearDown() {
        bookRepository.deleteById(100)
    }
}