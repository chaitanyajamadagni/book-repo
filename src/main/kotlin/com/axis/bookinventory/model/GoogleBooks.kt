package com.axis.bookinventory.model


data class GoogleBooks(val items: List<Item>)

data class Item(val volumeInfo: Book?)
