package com.axis.bookinventory.model

import org.springframework.data.mongodb.core.mapping.Document

@Document(collection = "Books")
data class Book(val id:Int,
                var title:String?,
                var authors:List<String>?,
                var imageLinks: ImageLinks?,
                var description:String?,
                var price:Double?,
                var quantity:Int?)

data class ImageLinks(val thumbnail: String?)