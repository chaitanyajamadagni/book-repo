package com.axis.bookinventory.controller

import com.axis.bookinventory.model.Book
import com.axis.bookinventory.model.GoogleBooks
import com.axis.bookinventory.repository.BookRepository
import com.axis.bookinventory.service.Consumer
import com.axis.bookinventory.service.GoogleBookService
import com.axis.bookinventory.service.Producer
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import reactor.core.publisher.Mono
import reactor.core.publisher.Flux
import java.time.Instant
import java.time.ZoneOffset
import java.time.format.DateTimeFormatter

@CrossOrigin(origins = ["*"], allowedHeaders = ["*"])
@RestController
@RequestMapping("/books")
class BookInventoryController(@Autowired var bookRepository: BookRepository,
                              @Autowired var googleBookService: GoogleBookService,
                              @Autowired var producer: Producer,
                              @Autowired var consumer: Consumer) {

    @GetMapping("/{id}")
    fun getBook(@PathVariable("id") id: Int): Mono<ResponseEntity<Book>> =
            bookRepository.findById(id)
                    .map { book -> ResponseEntity.ok(book) }
                    .defaultIfEmpty(ResponseEntity.notFound().build())

    @GetMapping("/search")
    fun searchBook(@RequestParam("key") key: String): Flux<Book> {
        return bookRepository.findByTitleIsLikeOrDescriptionIsLikeOrAuthorsLike(key,key,key)
        /*        .flatMap {searchedBooks ->
                    ResponseEntity.ok(searchedBooks)
                }
                .defaultIfEmpty(ResponseEntity.notFound().build())*/
    }

    @GetMapping
    fun getAllBooks(): Flux<Book> = bookRepository.findAll()

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    fun addBook(@ModelAttribute book: Book): Mono<Book> {
        this.producer.sendMessage("Book Added --> ${book.id} " +
                "--> ${book.title} --> ${book.quantity} --> ${book.price} --> ${DateTimeFormatter
                        .ofPattern("yyyy-MM-dd HH:mm:ss.SSSSSS")
                        .withZone(ZoneOffset.UTC)
                        .format(Instant.now())}")
        return bookRepository.save(book)
    }

    @PutMapping("/{id}")
    fun editBook(@PathVariable("id") id: Int, @RequestBody book: Book): Mono<ResponseEntity<Book>> {
        this.producer.sendMessage("Book Edited --> ${book.id} " +
                "--> ${book.title} --> ${book.quantity} --> ${book.price} --> ${DateTimeFormatter
                        .ofPattern("yyyy-MM-dd HH:mm:ss.SSSSSS")
                        .withZone(ZoneOffset.UTC)
                        .format(Instant.now())}")
        return bookRepository.findById(id)
                .flatMap { existingBook ->
                    updateBookAuthor(existingBook, book)
                            .map { ResponseEntity.ok(it) }
                }
                .defaultIfEmpty(ResponseEntity.notFound().build())
    }

    private fun updateBookAuthor(existingBook: Book, book: Book): Mono<Book> {
        existingBook.authors = book.authors
        existingBook.description = book.description
        existingBook.imageLinks = book.imageLinks
        existingBook.price = book.price
        existingBook.title = book.title
        existingBook.quantity = book.quantity
        return bookRepository.save(existingBook)
    }

    @DeleteMapping("/{id}")
    fun deleteBook(@PathVariable("id") id: Int) : Mono<ResponseEntity<Void>> {
        return bookRepository.findById(id)
                .flatMap {
                    existingBook ->deleteLogBook(existingBook)
                        .then(Mono.just(ResponseEntity.ok().build<Void>()))
                }
                .defaultIfEmpty(ResponseEntity.notFound().build())

    }

    private fun deleteLogBook(book: Book): Mono<Void> {
        this.producer.sendMessage("Book Deleted --> ${book.id} " +
                "--> ${book.title} --> ${book.quantity} --> ${book.price} --> ${DateTimeFormatter
                        .ofPattern("yyyy-MM-dd HH:mm:ss.SSSSSS")
                        .withZone(ZoneOffset.UTC)
                        .format(Instant.now())}")
        return bookRepository.delete(book)
    }

    @GetMapping("/google/{query}")
    fun getBooksFromGoogleApi(@PathVariable("query") query: String) : Flux<GoogleBooks>{
        return googleBookService.getBooksDetail(query)
    }

    @GetMapping("/logs")
    fun getLogsFromKafka() : List<String>{
    //    return Flux.just(Consumer.messages)
        return Consumer.messages;
    }

}
