package com.axis.bookinventory.service

import com.axis.bookinventory.model.GoogleBooks
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import org.springframework.web.reactive.function.client.WebClient
import org.springframework.web.util.UriComponentsBuilder
import reactor.core.publisher.Flux

@Service
class GoogleBookService(@Value("\${googleBooks.apiKey}") var apiKey: String,
                        @Value("\${googleBooks.endPoint}") var apiEndpoit: String) {

    fun getBooksDetail(query: String): Flux<GoogleBooks> {
        return WebClient
                .create(UriComponentsBuilder.fromHttpUrl(apiEndpoit)
                        .replaceQueryParam("q", query)
                        .replaceQueryParam("key", apiKey)
                        .encode().toUriString())
                .get()
                .retrieve()
                .bodyToFlux(GoogleBooks::class.java)
    }

}