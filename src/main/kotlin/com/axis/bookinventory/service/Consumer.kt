package com.axis.bookinventory.service

import org.slf4j.LoggerFactory
import org.springframework.kafka.annotation.KafkaListener
import org.springframework.stereotype.Service

@Service
class Consumer {
    private val logger = LoggerFactory.getLogger(Consumer::class.java)
    @KafkaListener(topics = ["testTopic"], groupId = "group_id")
    fun consume(message: String) {
        messages.add(message)
        logger.info(String.format("$$ -> Consumed Message -> %s", message))
    }
    companion object {
        val messages : MutableList<String> = mutableListOf()
    }
}