package com.axis.bookinventory.repository

import com.axis.bookinventory.model.Book
import org.springframework.data.mongodb.repository.ReactiveMongoRepository
import org.springframework.stereotype.Repository
import reactor.core.publisher.Flux

@Repository
interface BookRepository : ReactiveMongoRepository<Book,Int>{
    fun findByTitleIsLikeOrDescriptionIsLikeOrAuthorsLike(title:String,description:String,authors:String): Flux<Book>
}